## Coding style

目前使用套件如下，基底 Coding Style 使用 [Airbnb Style](https://github.com/airbnb/javascript)，另外因為 `create-react-app` 本身已自帶 **ESLint**，但版本過舊無法使用相關套件，所以需再 `.env` 檔案裡加上 `SKIP_PREFLIGHT_CHECK=true` 來取代原本的 **ESLint** 。

```json
"devDependencies": {
    "eslint": "7",
    "eslint-config-airbnb": "^19.0.4",
    "eslint-config-prettier": "^8.3.0",
    "eslint-plugin-import": "^2.25.4",
    "eslint-plugin-jsx-a11y": "^6.5.1",
    "eslint-plugin-prettier": "^4.0.0",
    "eslint-plugin-react": "^7.28.0",
    "prettier": "^2.5.1"
}
```

希望對於 ESLint 的提示訊息建立下列兩點共識:

- warning (黃底提示): 可因方便開發或其他個人因素留存，若可排除則盡量排除。
- error (紅底提示): 排除全部 Error。

<hr>

目前在我大略觀察專案之前的 Coding Style 後，得出下方較合適的設定，某些會出 Error 的地方，為求開發便利改為以 warning 方式呈現。

```json
"rules": {
    "prettier/prettier": ["error", prettierOptions],
    "react/jsx-filename-extension": [1, { extensions: [".js", ".jsx"] }],
    "react/prop-types": "off", // 關閉 prop type 檢查
    "max-lines": "off", // 關閉檔案最多 300 行
    "no-unused-vars": "warn", // 未使用變數
    camelcase: "warn", // 駝峰式命名
    "no-nested-ternary": "warn", // 巢狀判斷式
    "no-use-before-define": "warn", // 使用前需定義
    "import/order": "warn", // import 順序
    "global-require": "warn", // require 需要置頂
    "default-param-last": "warn", // func param 若有預設值需擺在最後
    "no-shadow": "warn", // 參數與全域變數等盡量不要重複名字
}
```

下方針對 prettier 設定做解釋，另外設定完成後可重新啟用 prettier format on save 功能

```json
{
  "singleQuote": true,
  "trailingComma": "all",
  "printWidth": 80,
  "tabWidth": 2,
  "useTabs": false,
  "semi": true
}
```

- 全部使用單引號
- 能在結尾加逗號的地方就加 ( PR 在查看 Diff 時較清楚，不會多換行 )
- printWidth 80 較適合 prettier 套件
- 結尾皆要使用分號
