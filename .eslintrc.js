const fs = require('fs');
const path = require('path');

const prettierOptions = JSON.parse(
  fs.readFileSync(path.resolve(__dirname, '.prettierrc'), 'utf8'),
);

module.exports = {
  env: {
    browser: true,
    jest: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'airbnb',
    'prettier',
  ],
  plugins: ['react', 'prettier'],
  rules: {
    'prettier/prettier': ['error', prettierOptions],
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'react/prop-types': 'off',
    'max-lines': 'off',
    'no-unused-vars': 'warn',
    camelcase: 'warn',
    'no-nested-ternary': 'warn',
    'consistent-return': 'warn',
    'no-use-before-define': 'warn',
    'import/order': 'warn',
    'global-require': 'warn',
    'default-param-last': 'warn',
    'no-shadow': 'warn',
  },
};
